class MasterOfTones {
    constructor(output_text_field) {
        this.CHORDS = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "B", "H"];
        this.out = output_text_field;
        this.ins = [];
        this.rightAnswer = "";
    }
    toneShift(basic, shift) {
        let target = this.CHORDS.indexOf(basic) + shift;
        while (target < 0)  {target += 12}
        while (target > 12) {target -= 12}
        return this.CHORDS[target];
    }
    getRandomTone() {
        return this.CHORDS[Math.floor(Math.random()*11.9)];
    }
    getDifferentRandomTones(main, amount) {
        var chosen_tones = [main]
        for (let i = 1; i < amount; i++) {
            let candidate = this.getRandomTone();
            while ((chosen_tones.indexOf(candidate) >= 0) || (candidate === undefined)) {
                candidate = this.toneShift(candidate, 1);
            }
            chosen_tones.push(candidate);
        }
        return chosen_tones;
    }
    print(tone, shift) {
        let middle = (Math.sign(shift) == -1) ? " - " : " + ";
        this.out.innerHTML = tone + middle + Math.abs(shift);
    }
    initButton(button) {
        button.addEventListener("click", (e) => {
            this.vote(e.target.innerHTML);
        })
    }
    addButton(button_object) {
        this.ins.push(button_object);
        this.initButton(button_object);
    }
    vote(answer) {
        console.log("ANSWER: ", answer, "\tRIGHT: ", this.rightAnswer);
        if (answer == this.rightAnswer) {
            this.right();
        } else {
            this.wrong();
        }
    }
    right() {
        console.log("right");
        this.out.classList.add("right");
        setTimeout(() => {
            this.newQuestion();
        }, 250);
    }
    wrong() {
        console.log("wrong");
        this.out.classList.add("wrong");
        setTimeout(() => {
            this.newQuestion();
        }, 250);
    }

    clear() {
        this.out.classList.remove("right");
        this.out.classList.remove("wrong");
    }

    newQuestion() {
        this.clear();
        let main = this.getRandomTone();
        let shift = Math.round((Math.random() - 0.5) * 24);
        this.print(main, shift);
        this.rightAnswer = this.toneShift(main, shift);
        let answers = this.getDifferentRandomTones(this.rightAnswer, this.ins.length);
        answers.sort();
        console.log("ANSWERS: ", answers, "SHIFT: ", shift);
        for (let i = 0; i < this.ins.length; i++) {
            this.ins[i].innerText = answers[i];
        }
    }
}